/*
 * Copyright (c) 2022 PebblePost Inc.
 * All rights reserved.
 */

package com.pebblepost.challenges.countspecial;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SolutionTest {

    Map<Integer, Integer> preCalculatedValues = Stream.of(new Integer[][]{
            {20, 9},
            {53, 30},
            {65, 42},
            {367, 272},
            {7564, 6557},
            {10000, 8704},
            {7, 3},
            {1, 0},
            {9999, 8704},
            {222, 151},
            {500, 357}
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    @Test
    void countSpecial_1() {
        assertThat(new Solution().countSpecial(12)).isEqualTo(5);
    }
    
    @Test
    void countSpecial_2() {
        assertThat(new Solution().countSpecial(14)).isEqualTo(5);
    }
    
    @Test
    void countSpecial_3() {
        assertThat(new Solution().countSpecial(15)).isEqualTo(6);
    }

    @Test
    void countSpecial_4() {
        Solution solution = new Solution();
        preCalculatedValues.entrySet().stream()
                .forEach(entry -> assertThat(solution.countSpecial(entry.getKey()))
                        .isEqualTo(entry.getValue()));
    }

}
