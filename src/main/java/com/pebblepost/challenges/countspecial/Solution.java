/*
 * Copyright (c) 2022 PebblePost Inc.
 * All rights reserved.
 */

package com.pebblepost.challenges.countspecial;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Implements special count logic
 */
public class Solution {

    //Special digits: [1 ... 9] - (do not use zero: not implemented and will be wrong calculation)
    public static final int[] SPECIAL_DIGITS = {2, 5, 6, 9};
    //Max number to restrict special count
    public static final int MAX_NUMBER = 10000;


    //if some digit is special - rights[digit] will be true
    private final boolean[] rights = new boolean[10];
    //predefined special counts
    private final int[] counts = new int[10];

    public Solution() {
        IntStream.range(0, 10).forEach(i -> rights[i] = Arrays.stream(SPECIAL_DIGITS).anyMatch(sp -> sp == i));
        IntStream.range(0, 10).forEach(i -> counts[i] = (i > 0 ? counts[i - 1] : 0) + (rights[i] ? 1 : 0));
    }

    /**
     * Returns the count of special numbers in the range  [1 ... n]
     * @param {int} n - integer range limit [1 ... n]
     * @return the count of numbers with the special digits
     */
    public int countSpecial(int n) {
        if (n < 1 || n > MAX_NUMBER) {
            throw new RuntimeException(String.format("n must be in the range [1 . . . %d]", MAX_NUMBER));
        }

        //convert integer to digits array
        int digits[] = Integer.toString(n).chars().map(c -> c - '0').toArray();
        int len = digits.length;

        //first special digit index
        int firstSpecialInd = IntStream.range(0, len)
                .filter(i -> rights[digits[i]])
                .findFirst()
                .orElse(-1);

        //current digit degree: 1, 10, 100, ...
        int degree = 1;
        //special counts for current degree
        int degreeCount = 0;
        //result will be return
        int resultCount = 0;

        //current digit, special count, shift to correct degree
        int digit, cDigit, shift;

        //iterate digits from right to left
        for (int i = len - 1; i >= 0; i--) {
            digit = digits[i];
            //check for special upper digit or special current digit
            if (firstSpecialInd < 0 || firstSpecialInd >= i) {
                cDigit = counts[digit];
                shift = rights[digit] ? 1 : 0;
                resultCount += (cDigit - shift) * degree + (digit - cDigit + shift) * degreeCount + shift;
            } else {
                //just sum lower digit because there is special upper digit
                resultCount += digit * degree;
            }

            //calculate degree for next iteration
            if (i > 0) {
                degreeCount = SPECIAL_DIGITS.length * degree + (10 - SPECIAL_DIGITS.length) * degreeCount;
                degree *= 10;
            }
        }

        return resultCount;
    }
}
